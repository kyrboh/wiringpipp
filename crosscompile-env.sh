#!/bin/bash

function cmake() {
  local project_dir=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)
  echo "cmake -DCMAKE_TOOLCHAIN_FILE='$project_dir/cmake/RaspbianToolchain.cmake' $@"
  command cmake -DCMAKE_TOOLCHAIN_FILE="$project_dir/cmake/RaspbianToolchain.cmake" "$@"
}
