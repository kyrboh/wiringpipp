#include "wpi/pin.hpp"

#include "wpi/util/enum.hpp"
#include "wpi/util/float.hpp"

#include <pthread.h>
#include <wiringPi.h>

namespace wpi {

Pin::Pin(const uint8_t number, const PinMode mode) :
  m_number{number},
  m_mode{mode}
{
  ::pinMode(number, util::enum_cast(mode));
}

void DigitalOutPin::write(const bool value) {
  ::digitalWrite(Pin::number(), static_cast<int>(value));
}

void DigitalInPin::pull(const DigitalPinPull pull) {
  ::pullUpDnControl(Pin::number(), util::enum_cast(pull));
}

bool DigitalInPin::read() const {
  return static_cast<bool>(::digitalRead(Pin::number()));
}

void PwmPin::write(const std::uint16_t value) {
  ::pwmWrite(Pin::number(), value);
}

SoftPwmPin::Percent SoftPwmPin::dutyCycle() {
  return m_dutyCycle.load(std::memory_order_consume);
}

void SoftPwmPin::setDutyCycle(const Percent dutyCycle) {
  if (dutyCycle < MIN_DUTY_CYCLE || dutyCycle > MAX_DUTY_CYCLE)
    throw std::invalid_argument("invalid duty cycle value");

  m_dutyCycle.store(dutyCycle, std::memory_order_release);
}

void SoftPwmPin::setPeriod(const Duration period)
{
  const bool wasRuning = stop();
  m_period = period;

  if (wasRuning) {
    start();
  }
}

void SoftPwmPin::start() {
  if (!isRunning()) {
    m_thread = std::thread{SoftPwmPin::softPwmThread, std::ref(*this)};
  }
}

bool SoftPwmPin::stop() {
  if (!isRunning())
    return false;

  if (!m_thread.joinable()) {
    throw std::runtime_error("soft PWM thread runs but is not joinable");
  }

  // flag thread to exit with an invalid duty cycle value
  m_dutyCycle.store(MAX_DUTY_CYCLE + 1, std::memory_order_release);
  m_thread.join();
  return true;
}

bool SoftPwmPin::isRunning() const {
  return m_thread.get_id() != std::thread::id{};
}

SoftPwmPin::~SoftPwmPin() {
  stop();
}

void SoftPwmPin::softPwmThread(SoftPwmPin &pin) {
  //! \note std::thread does not allow for setting priority
  {
    sched_param scheduleParams;
    const auto self = pthread_self();
    int policy = 0;

    pthread_getschedparam(self, &policy, &scheduleParams);
    scheduleParams.sched_priority = sched_get_priority_max(SCHED_RR);
    pthread_setschedparam(self, SCHED_RR, &scheduleParams);
  }

  const auto period = pin.m_period;
  if (period.count() == 0) {
    throw std::runtime_error("PWM period cannot be zero");
  }

  while (true) {
    const auto dutyCycle = pin.m_dutyCycle.load(std::memory_order_consume);
    if (dutyCycle < MIN_DUTY_CYCLE || dutyCycle > MAX_DUTY_CYCLE) {
      break;
    }

    const auto highPart = period * (dutyCycle / 100);
    const auto lowPart = period - highPart;

    // avoid unnecessary flickering by checkign the value inbefore
    if (highPart.count() > 0) {
      static_cast<DigitalOutPin&>(pin).write(true);
    }
    std::this_thread::sleep_for(highPart);

    if (lowPart.count() > 0) {
      static_cast<DigitalOutPin&>(pin).write(false);
    }
    std::this_thread::sleep_for(lowPart);
  }
}

} // namespace wpi
