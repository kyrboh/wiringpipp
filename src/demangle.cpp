#include "wpi/util/demangle.hpp"

#include <memory>
#include <cstdlib>

#include <cxxabi.h>

namespace wpi::util {

std::string demangle(const char *mangled) {

  int status = 0;
  std::unique_ptr<char, decltype(&std::free)> chars{
    ::abi::__cxa_demangle(mangled, nullptr, nullptr, &status),
    std::free
  };

  return status == 0 ? chars.get() : mangled;
}

} // namespace wpi::util
