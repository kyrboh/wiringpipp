#include "wpi/board.hpp"

#include "wpi/util/enum.hpp"

#include <wiringPi.h>

#include <cstdlib>
#include <cstring>
#include <stdexcept>
#include <string>

namespace wpi::detail {
std::atomic<bool> PrivateBoardBase::isInitialized{false};

PrivateBoardBase::PrivateBoardBase() {
  bool wasInitialized = false;
  isInitialized.compare_exchange_strong(wasInitialized, true,
                                        std::memory_order_release,
                                        std::memory_order_acquire);

  if (wasInitialized)
    return;

  if (::setenv("WIRINGPI_CODES", "1", 1) != 0) {
    auto reason = std::string{"failed to set WIRINGPI_CODES env: "} +
                  std::strerror(errno);
    throw std::runtime_error(reason);
  }

  // NOTE: since v2 setup fail causes fatal error unless env is set
  if (const int err = ::wiringPiSetup(); err != 0) {
    throw std::runtime_error("failed to setup wiringPi: " + std::to_string(err));
  }
}

Pin &PrivateBoardBase::pin(std::unique_ptr<Pin> &place,
                           const std::uint8_t number,
                           const PinMode mode) {
  if (!place || place->mode() != mode) {
    switch (mode) {
    case PinMode::DIGITAL_IN:
      place.reset(new DigitalInPin{number});
      break;
    case PinMode::DIGITAL_OUT:
      place.reset(new DigitalOutPin{number});
      break;
    case PinMode::PWM:
      place.reset(new PwmPin{number});
      break;
    case PinMode::SOFT_PWM:
      place.reset(new SoftPwmPin{number});
      break;
    default:
      throw std::runtime_error{"unknown pin mode: " + std::to_string(util::enum_cast(mode))};
    }
  }

  return *place;
}

} // namespace wpi::detail
