#pragma once

#include <limits>
#include <cmath>
#include <type_traits>

namespace wpi::util {

namespace detail {
constexpr unsigned int EPSILON_MULTIPLIER = 3;
}

template <typename Float>
constexpr std::enable_if_t<std::is_floating_point_v<Float>, bool>
isZero(const Float value) {
  return std::abs(value)
      < std::numeric_limits<Float>::epsilon() * detail::EPSILON_MULTIPLIER;
}

template <typename LhsFloat, typename RhsFloat>
constexpr std::enable_if_t<std::is_floating_point_v<LhsFloat> &&
                           std::is_floating_point_v<RhsFloat>, bool>
areEqual(const LhsFloat lhs, const RhsFloat rhs) {
  return isZero(lhs - rhs);
}


} // namespace wpi::util
