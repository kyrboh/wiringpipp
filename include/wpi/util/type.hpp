#pragma once

#include <wpi/util/demangle.hpp>

#include <typeinfo>

namespace wpi::util {

template <typename T>
std::string type() {
  return util::demangle(typeid(T).name());
}

} // namespace wpi::util
