#pragma once

#include <type_traits>

namespace wpi::util {

template <typename Enum>
constexpr auto enum_cast(Enum v) {
  return static_cast<std::underlying_type_t<Enum>>(v);
}

} // namespace wpi::util
