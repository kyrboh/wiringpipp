#pragma once

#include <string>

namespace wpi::util {

std::string demangle(const char *mangled);

} // namespace wpi::util
