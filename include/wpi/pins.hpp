#pragma once

#include <wiringPi.h>

#include <cstdint>

namespace wpi {

//! WiringPi++ main (and only) pin number schema
//!
//! To avoid uncecessary confusion support only the WiringPi
//! pinning schema
//!
//! \todo consider making enum type name more explicit, but not too long
enum class Out : std::uint8_t {
  GPIO_0 = 0,
  GPIO_1 = 1,
  GPIO_2 = 2,
  GPIO_3 = 3,
  GPIO_4 = 4,
  GPIO_5 = 5,
  GPIO_6 = 6,
  GPIO_7 = 7,
  GPIO_26 = 26,
  GPIO_27 = 27,
};

enum class In : std::uint8_t {
  GPIO_0 = 0,
  GPIO_1 = 1,
  GPIO_2 = 2,
  GPIO_3 = 3,
  GPIO_4 = 4,
  GPIO_5 = 5,
  GPIO_6 = 6,
  GPIO_7 = 7,
  GPIO_26 = 26,
  GPIO_27 = 27,
};

//! \todo sort out PWM channels
//!       because GPIOs  1,26 are connected to channel 0
//!           and GPIOs 23,24 are connected to channel 1
//!       meaning that any value written to a channel actually
//!       may appear on two pins
enum class Pwm : std::uint8_t {
  GPIO_1 = 1, // channel 0
  GPIO_23 = 23, // channel 1
  GPIO_24 = 24, // channel 1
  GPIO_26 = 26, // channel 0
};

enum class SoftPwm : std::uint8_t {
  GPIO_0 = 0,
  GPIO_1 = 1,
  GPIO_2 = 2,
  GPIO_3 = 3,
  GPIO_4 = 4,
  GPIO_5 = 5,
  GPIO_6 = 6,
  GPIO_7 = 7,
  GPIO_26 = 26,
  GPIO_27 = 27,
};

namespace detail {
#ifdef INPUT
constexpr std::uint8_t MODE_DIGITAL_IN = INPUT;
#undef INPUT
#endif // INPUT

#ifdef OUTPUT
constexpr std::uint8_t MODE_DIGITAL_OUT = OUTPUT;
#undef OUTPUT
#endif // OUTPUT

#ifdef PWM_OUTPUT
constexpr std::uint8_t MODE_PWM = PWM_OUTPUT;
#undef PWM_OUTPUT
#endif // PWM_OUTPUT

#ifdef SOFT_PWM_OUTPUT
constexpr std::uint8_t MODE_SOFT_PWM = SOFT_PWM_OUTPUT;
#undef SOFT_PWM_OUTPUT
#endif // SOFT_PWM_OUTPUT
} // namespace detail

enum class PinMode : std::uint8_t {
  DIGITAL_IN = detail::MODE_DIGITAL_IN,
  DIGITAL_OUT = detail::MODE_DIGITAL_OUT,
  PWM = detail::MODE_PWM,
  SOFT_PWM = detail::MODE_SOFT_PWM,
};

enum class DigitalPinPull : std::uint8_t {
  UP = PUD_UP,
  DOWN = PUD_DOWN,
  OFF = PUD_OFF,
};

} // namespace wpi
