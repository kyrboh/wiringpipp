#pragma once

#include "wpi/pins.hpp"

#include <atomic>
#include <chrono>
#include <thread>

namespace wpi {
namespace detail { class PrivateBoardBase; }

class Pin {
public:
  //! \todo
  Pin() = delete;
  Pin(const Pin&) = delete;
  Pin(Pin&&) = delete;

  constexpr auto mode() const { return m_mode; }
  constexpr auto number() const { return m_number; }

protected:
  explicit Pin(const std::uint8_t number, const PinMode mode);

private:
  const std::uint8_t m_number;
  PinMode m_mode;
};

class DigitalOutPin : public Pin {
public:
  void write(const bool value);

protected:
  friend class detail::PrivateBoardBase;
  explicit DigitalOutPin(const std::uint8_t number) : Pin{number, PinMode::DIGITAL_OUT} {}
};

class DigitalInPin : public Pin {
public:
  void pull(const DigitalPinPull pull);
  bool read() const;

protected:
  friend class detail::PrivateBoardBase;
  explicit DigitalInPin(const std::uint8_t number) : Pin{number, PinMode::DIGITAL_IN} {}
};

class PwmPin : public Pin {
public:
  void write(const std::uint16_t value);

protected:
  friend class detail::PrivateBoardBase;
  explicit PwmPin(const std::uint8_t number) : Pin{number, PinMode::PWM} {}
};

class SoftPwmPin : public DigitalOutPin {
public:
  //! granularity is 100us
  using Duration = std::chrono::duration<std::uintmax_t,
                                         std::ratio_multiply<std::ratio<100, 1>,
                                                             std::micro>>;
  using Percent = double;

  //! Alias to setDutyCycle(), for consistency
  void write(const Percent dutyCycle) { setDutyCycle(dutyCycle); }

  Percent dutyCycle();
  void setDutyCycle(const Percent dutyCycle);

  auto period() const noexcept { return m_period; }

  template <typename T>
  void setPeriod(const T period) {
    setPeriod(std::chrono::duration_cast<Duration>(period));
  }

  void setPeriod(const Duration period);

  void start();
  bool stop();
  bool isRunning() const;

  ~SoftPwmPin();

protected:
  friend class detail::PrivateBoardBase;
  explicit SoftPwmPin(const std::uint8_t number) : DigitalOutPin{number} {
    // \todo: seems to be not needed
    DigitalOutPin::write(false);
  }

private:
  static void softPwmThread(SoftPwmPin &pin);

  constexpr static Percent MAX_DUTY_CYCLE = 100;
  constexpr static Percent MIN_DUTY_CYCLE = 0;

  std::thread m_thread;
  std::atomic<Percent> m_dutyCycle = 50;
  Duration m_period{100};
};

} // namespace wpi
