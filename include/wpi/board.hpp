#pragma once

#include <wpi/pin.hpp>
#include <wpi/pins.hpp>
#include <wpi/util/enum.hpp>

#include <array>
#include <atomic>
#include <memory>

namespace wpi {
namespace detail {

class PrivateBoardBase {
protected:
  explicit PrivateBoardBase();

  static Pin &pin(std::unique_ptr<Pin> &place, std::uint8_t number, const PinMode mode);

private:
  static std::atomic_bool isInitialized;
};

} // namespace detail


//! \todo make this singleton
template <std::size_t npins>
class Board : protected detail::PrivateBoardBase {
public:
  explicit Board() = default;

  Board(const Board &) = delete;
  Board(Board&&) = delete;

  DigitalOutPin &pin(const Out id) {
    const auto number = util::enum_cast(id);
    return static_cast<DigitalOutPin &>(PrivateBoardBase::pin(pins[number], number, PinMode::DIGITAL_OUT));
  }

  DigitalInPin &pin(const In id) {
    const auto number = util::enum_cast(id);
    return static_cast<DigitalInPin &>(PrivateBoardBase::pin(pins[number], number, PinMode::DIGITAL_IN));
  }

  PwmPin &pin(const Pwm id) {
    const auto number = util::enum_cast(id);
    return static_cast<PwmPin &>(PrivateBoardBase::pin(pins[number], number, PinMode::PWM));
  }

  SoftPwmPin &pin(const SoftPwm id) {
    const auto number = util::enum_cast(id);
    return static_cast<SoftPwmPin &>(PrivateBoardBase::pin(pins[number], number, PinMode::SOFT_PWM));
  }

private:
  std::array<std::unique_ptr<Pin>, npins> pins;
};

class RaspberryPi3BPlus : public Board<40> {};

} // namespace wpi
