WiringPi++
==========

A C++ wrapper for the original WiringPi library. The main purpose of this project is to
provide an interface to the original library written in C with the means of modern C++.

I have put this library together as a side-product while experimenting with a RPi board
of mine. While it does not do much, it might prove useful to somebody. Or (as I would hope)
develop into something bigger.

Features
========

* C++ enums to identify pin type to use with overloaded functions
* Latest and greatest C++17 features are allowed
* Can produce a handy .deb package (utilizing CPack)
* Can be easily cross-compiled (utilizing CMake)

Cross-Compilation
=================

For easy cross-compilation a helper file cmake/RaspbianToolchain.cmake is included as part of
this project. The file tells CMake to use arm-linux-gnueabihf-g++ as C++ compiler. Such a compiler
might be obtained on Debian-based distros by installing g++-arm-linux-gnueabihf package.

To compile the project with an arbitrary toolchain use CMAKE_TOOLCHAIN_FILE CMake's option when
configuring the project initially:

  cmake -DCMAKE_TOOLCHAIN_FILE=/path/to/your/ToolchainFile.cmake /path/to/wiringpi++/sources

There is also a crosscompile-env.sh script that replaces cmake command with a shell function
which effectively calls cmake binary and specifies to RaspbianToolchain.cmake as toolchain file
as the first option to cmake. This way there is no need to remember to add -DCMAKE_TOOLCHAIN_FILE
when actively using CMake.

Hacking
=======

1. Header naming convention: in order not to overlap the original WiringPi headers, all the C++
   headers should have a .hpp extension
