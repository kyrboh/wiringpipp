cmake_minimum_required(VERSION 3.7)

function(enable_all_warnings)
  if (CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
    add_compile_options(
      -Weverything
      -Wno-c++98-compat
      -Wno-c++98-compat-pedantic
      -Wno-missing-prototypes
      -Wno-covered-switch-default
    )
  elseif (CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
    add_compile_options(
      -Wall
      -Wextra
      -Wpedantic
      -Wc++17-compat
      -Wconversion
      -Wcpp
      -Wctor-dtor-privacy
      -Wdeprecated
      -Wdisabled-optimization
      -Wdouble-promotion
      -Wduplicated-branches
      -Wduplicated-cond
      -Wignored-attributes
      -Winvalid-offsetof
      -Wlogical-op
      -Wmisleading-indentation
      -Wnon-virtual-dtor
      -Wnull-dereference
      -Wold-style-cast
      -Woverflow
      -Wpacked
      -Wshadow
      -Wunused
      -Wuseless-cast
      -Wzero-as-null-pointer-constant
      -Wpadded
    )
  else()
    message(WARNING "Compiler ${CMAKE_CXX_COMPILER_ID} is not supported, no additional warnings will be emitted")
  endif()
endfunction()
