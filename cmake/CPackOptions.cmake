if(CPACK_GENERATOR MATCHES "DEB")
  set(CPACK_DEBIAN_FILE_NAME DEB-DEFAULT)

  # The Debian package architecture. Default: Output of dpkg --print-architecture (or i386 if dpkg is not found)
  # TODO: it might be desired to generate an x86 package (for debug purposes)
  set(CPACK_DEBIAN_PACKAGE_ARCHITECTURE "armhf")

  set(CPACK_DEBIAN_PACKAGE_DEPENDS "wiringpi (>= 2.46)")
  set(CPACK_DEBIAN_PACKAGE_HOMEPAGE "https://github.com/elvenfighter/wiringpi++")
  set(CPACK_DEBIAN_PACKAGE_ENHANCES "wiringpi")

  # May be set to ON in order to use dpkg-shlibdeps to generate better package dependency list.
  # FIXME: this does not work for cross-compilation
  #set(CPACK_DEBIAN_PACKAGE_SHLIBDEPS ON)

  # Allows to generate shlibs control file automatically.
  set(CPACK_DEBIAN_PACKAGE_GENERATE_SHLIBS ON)

  # set(CPACK_DEBIAN_PACKAGE_CONTROL_EXTRA
  #     "${CMAKE_CURRENT_SOURCE_DIR}/prerm;${CMAKE_CURRENT_SOURCE_DIR}/postrm")
  #CPACK_DEBIAN_PACKAGE_CONTROL_EXTRA

  # This variable indicates if the Debian policy on control files should be strictly followed.
  set(CPACK_DEBIAN_PACKAGE_CONTROL_STRICT_PERMISSION TRUE)
endif()
